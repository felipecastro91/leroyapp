(function($, document, window, viewport){
	
	project.login = {
		init: function(name){
			var _this = project.login;
			_this.trigger();
		},

		trigger: function(){
			// project.login.sayHello();
			project.login.sendForm();
		},

		sendForm: function(){
			$('#formLogin button').on('click', function(){
				if( project.tools.validaForms2('#formLogin') ){
					if( ($('#lpda').val() == '1234') && ( $('#password').val() == '1234' )  ){
						
						project.tools.loaderMaster(true);

						setTimeout(function(){

							$('body').addClass('logged');
							$('#login').addClass('hidden');
							$('#menu').removeClass('hidden');
							project.tools.loaderMaster(false);


						}, 300)

					} else {
						alert('Error Login');
					}
				}

			});
		},

		sayHello: function(){
			alert('Hi');
		}
		
	};

	jQuery(document).ready(function($) {
		var _this = project.login;
		_this.init();
	});

})(jQuery, document, window);