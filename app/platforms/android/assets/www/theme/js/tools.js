(function($, document, window, viewport){
	
	project = [];
	project.tools = {
		init: function(name){
			var _this = project.tools;
			_this.trigger();
		},

		trigger: function(){
			var _this = project.tools;
			_this.getScan();
		},

		scan: function(){
			cordova.plugins.barcodeScanner.scan(
				function (result) {
					// alert("We got a barcode\n" +
					// 	"Result: " + result.text + "\n" +
					// 	"Format: " + result.format + "\n" +
					// 	"Cancelled: " + result.cancelled);
					$('#product').val(result.text);
					$('label[for="product"]').addClass('active');

				},
				function (error) {
					alert("Scanning failed: " + error);
				},
				{
					preferFrontCamera : false, 
					showFlipCameraButton : false, 
					showTorchButton : false, 
					torchOn: false, 
					saveHistory: true, 
					prompt : "Posicione o codígo de barra dentro da área.", 
					resultDisplayDuration: 500, 
					formats : "QR_CODE, DATA_MATRIX, UPC_A, UPC_E, EAN_8, EAN_13, CODE_39, CODE_93, CODE_128, CODABAR, ITF, RSS14, PDF417, RSS_EXPANDED, MSI, AZTEC", 
					orientation : "landscape", 
					disableAnimations : true, 
					disableSuccessBeep: false 
				}
				)
		},

		getScan: function(){
			$('#scan').on('click', project.tools.scan);
		},

		viewport: function(x){
			return viewport.is(x);
		},

		sizeOfWindow: function(){
			var windowWidth = window.innerWidth;
			var windowHeight = window.innerHeight;
			var screenWidth = screen.width;
			var screenHeight = screen.height;

			return {windowWidth: windowWidth, windowHeight: windowHeight, screenWidth: screenWidth, screenHeight: screenHeight};
		},

		loaderMaster:function(key){
			if (key == true){
				$("#loader").removeClass('hidden');
			}
			
			if (key == false){
				$("#loader").addClass('hidden');
			}
		},

		rmvLoaderPage: function(){
			$('#loaderToPage').addClass('hidden');
		},

		validaForms: function(idForm){
			$(idForm + " .inpt_error").removeClass('inpt_error');
			$(idForm + " .span_error").remove();
			$(idForm + " .select_box_error").removeClass('select_box_error');

			$("form"+idForm+" input[required=true]").each(function(index, data){

				if ( $(data).val() == '' ){
					$(data).addClass('inpt_error');
					$(data).after("<span id='error_"+$(data).attr('id')+"' class='span_error'>Preencha o campo "+ $(data).attr('data-lbkey') +"<span>");
				}

			});

			$("form"+idForm+" select[required=true]").each(function(index, data){

				if ( $(data).val() == '' || $(data).val() == null ){
					$(data).addClass('inpt_error');
					if( $(data).parent().hasClass('boxSelect') ){
						$(data).parent('.boxSelect').addClass('select_box_error');
						$(data).parent('.boxSelect').after("<span id='error_"+$(data).attr('id')+"' class='span_error'>Selecione o campo "+ $(data).attr('data-lbkey') +"<span>");
					} else {
						$(data).after("<span id='error_"+$(data).attr('id')+"' class='span_error'>Selecione o campo "+ $(data).attr('data-lbkey') +"<span>");
					}
				}

			});

			$("form"+idForm+" textarea[required=true]").each(function(index, data){

				if ( $(data).val() == '' || $(data).val() == null ){
					$(data).addClass('inpt_error');
					$(data).after("<span id='error_"+$(data).attr('id')+"' class='span_error'>Preencha o campo "+ $(data).attr('data-lbkey') +"<span>");
				}

			});

			if( $(idForm + " .inpt_error").length ){
				return false;
			} else {
				return true;
			}

		},

		validaForms2: function(idForm){
			var validForm = true;

			$("form"+idForm+" input[required=true]").each(function(index, data){
				if ( $(data).val() == '' ){
					validForm = false;
				}

			});

			$("form"+idForm+" select[required=true]").each(function(index, data){
				if ( $(data).val() == '' || $(data).val() == null ){
					validForm = false;
				}
			});

			$("form"+idForm+" textarea[required=true]").each(function(index, data){
				if ( $(data).val() == '' || $(data).val() == null ){
					validForm = false;
				}
			});

			return validForm;

		},

		validationLive: function(){
			var id = $(this).attr('id');
			$(this).parent().removeClass('has-error');

			if( $(this).siblings('.help-block').length ){
				$(this).siblings('.help-block').remove();
			}

			if($(this).val() && $(this).hasClass('inpt_error')){
				$(this).removeClass('inpt_error');
				$('#error_' +  id).remove();
				if( $(this).attr('type') == 'select' ){
					$(this).parent().removeClass('select_box_error');
				}
			}
		},

		isValidEmail:function(data){
			var email = $(data).val();
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(re.test(email)){
				return re.test(email);
			} else {
				$(data).addClass('inpt_error');
				$(data).after("<span id='error_"+$(data).attr('id')+"' class='span_error'>Preencha um E-mail válido.<span>");
			}
		},

		removeHiddenLoaderContent: function(){
			$('.bgcontent .bgLoader').removeClass('hidden');
		}
	};

	jQuery(document).ready(function($) {
		var _this = project.tools;
		_this.init();
	});

})(jQuery, document, window);