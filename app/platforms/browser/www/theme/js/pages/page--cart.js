(function($, document, window, viewport){
	
	project.cart = {
		init: function(name){
			var _this = project.cart;
			_this.trigger();
		},

		trigger: function(){
			// project.cart.sayHello();
			project.cart.sendForm();
			$('#teste').on('click', project.cart.callRest );
		},

		callRest: function(){
			$.ajax({
				url: 'http://rest-service.guides.spring.io/greeting',
				type: 'GET',
				dataType: 'JSON',
			})
			.done(function(data) {
				alert(data.content);
			})
			.fail(function(data, a, b) {
				alert('error')
			})
			.always(function() {
				alert("complete");
			});
		},

		sendForm: function(){
			$('#formSearchProduct button').on('click', function(){
				if( project.tools.validaForms2('#formSearchProduct') ){
					
					project.tools.loaderMaster(true);

					setTimeout(function(){

						var dataForm = { 
							cart: $('#formSearchProduct #cart').val(), 
							store: $('#formSearchProduct #store').val(), 
							product: $('#formSearchProduct #product').val(),
							quantity: '1' 
						};

						// var dataForm = { 
						// 	cart: "0000002667",
						// 	store: "0004",
						// 	product: "85952741",
						// 	// product: "87453506",
						// 	quantity: "1" 
						// };

						var auth = Base64.encode('admin:nimda');

						$.ajax({
							url: 'http://172.16.6.15:9001/lmrest/newEntry/byGet',
							type: 'GET',
							dataType: 'json',
							headers : function(xhr) {
								xhr.setRequestHeader('Authorization', 'Basic ' + 'YWRtaW46bmltZGE=');
								xhr.setRequestHeader('Content-Type','application/json');
							},
							xhrFields: {withCredentials: true},
							processData: true,
							data: dataForm
						})
						.done(function(data) {
							alert(data.header.responseMessage);
							
							$('#cart .emptyCart ').addClass('hidden');

							$(data.entries).each(function(index, el) {
								var $newItem = $('.templateItemProduct').clone(true, true);	
								$newItem.removeClass('templateItemProduct').removeClass('hidden');
								$newItem.addClass('itemProduct');
								$newItem.find('.nm_product').html(el.product)
								$newItem.find('.price').html(el.basePrice)


								$('#cart .cart').append($newItem);	
							});

							// $('#cart').addClass('hidden');
							// $('#pdp').removeClass('hidden');
						})
						.fail(function(data) {
							alert(data.responseJSON.header.responseMessage);
						})
						.always(function() {
							project.tools.loaderMaster(false);
						});

						
					}, 300)

				}
			});
		},
		
	};

	jQuery(document).ready(function($) {
		var _this = project.cart;
		_this.init();
	});

})(jQuery, document, window);