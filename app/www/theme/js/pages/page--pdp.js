(function($, document, window, viewport){
	
	project.pdp = {
		init: function(name){
			var _this = project.pdp;
			_this.trigger();
		},

		trigger: function(){
			$('#formAddToCart button').on('click', project.pdp.sendForm );
			$('#minus').on('click', project.pdp.qtdeMinus );
			$('#plus').on('click', project.pdp.qtdePlus );
		},

		qtdeMinus: function(){
			var qtde = $('#qtdeProduct').val();

			if(qtde != 1){
				qtde = parseInt(qtde) - 1;
			}

			$('#qtdeProduct').val(qtde);
		},

		qtdePlus: function(){
			var qtde = $('#qtdeProduct').val();
			qtde = parseInt(qtde) + 1;
			$('#qtdeProduct').val(qtde);
		},

		sendForm: function(){
			var data = $('#formAddToCart').serialize;

			project.tools.loaderMaster(true);
			setTimeout(function(){
				$('#pdp, .emptyCart').addClass('hidden');
				$('#cart, .itemProduct').removeClass('hidden');

				project.tools.loaderMaster(false);
			}, 300)
		},
		
	};

	jQuery(document).ready(function($) {
		var _this = project.pdp;
		_this.init();
	});

})(jQuery, document, window);