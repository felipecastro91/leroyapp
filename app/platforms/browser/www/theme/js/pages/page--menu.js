(function($, document, window, viewport){
	
	project.menu = {
		init: function(name){
			var _this = project.menu;
			_this.trigger();
			jQuery.support.cors = true;
		},

		trigger: function(){
			$('#gotoCart').on('click', project.menu.gotoCart );
			$('#ipconfig').on('click', project.menu.ipConfig );
		},

		ipConfig: function(){
			$('.spaceConfig').toggleClass('hidden');
		},

		gotoCart: function(){

			project.tools.loaderMaster(true);

			setTimeout(function(){
				
				var dataForm = { customer: '06903400958', store: '0004', seller: '51017240' };
				var auth = Base64.encode('admin:nimda');

				$.ajax({
					url: 'http://172.16.6.15:9001/lmrest/createCart/byGet',
					type: 'GET',
					dataType: 'json',
					headers : function(xhr) {
						xhr.setRequestHeader('Authorization', 'Basic ' + 'YWRtaW46bmltZGE=');
						xhr.setRequestHeader('Content-Type','application/json');
						xhr.setRequestHeader('Origin','http://myapp.fh.com.br');
					},
					xhrFields: {withCredentials: true},
					crossDomain: true,
					processData: true,
					data: dataForm
				})
				.done(function(data) {
					$('#formSearchProduct #cart').val(data.cartCode);
					$('#cart #numCart').html(data.cartCode);
					// $('#formSearchProduct label[for="code"]').addClass('active')
					$('#menu').addClass('hidden');
					$('#cart').removeClass('hidden');
				})
				.fail(function(data) {
					alert(data.responseJSON.header.responseMessage); 
				})
				.always(function() {
					project.tools.loaderMaster(false);
				});
				
			}, 300)

		}

	};

	jQuery(document).ready(function($) {
		var _this = project.menu;
		_this.init();
	});

})(jQuery, document, window);