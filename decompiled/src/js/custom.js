(function($, document, window, viewport){
	
	project.custom = {
		init: function(name){
			var _this = project.custom;
			_this.trigger();
		},

		trigger: function(){
			project.custom.noSendForm();
			$('#logout').on('click', project.custom.logout);

		},

		noSendForm: function(){
			$('form').on('submit', function(e){
				e.preventDefault();
			})
		},

		logout: function(){
			
			project.tools.loaderMaster(true);

			setTimeout(function(){
				$('body').removeClass('logged');
				$('.screen').addClass('hidden');
				$('#login').removeClass('hidden');
				$('#login form input').val('');
				$('#login form label').removeClass('active');
				project.tools.loaderMaster(false);

			}, 300)
		}
		
	};

	jQuery(document).ready(function($) {
		var _this = project.custom;
		_this.init();
	});

})(jQuery, document, window);